MINION4.AssetLoader = function() {
};

MINION4.AssetLoader.prototype = {
	constructor: MINION4.AssetLoader,

	load: function() {
		Crafty.sprite(16, 16, 'assets/sprites/lava.png', {
			LavaSprite: [0, 0]
		}, 1, 1);
		
		Crafty.sprite(16, 16, 'assets/sprites/stone.png', {
			StoneSprite: [0, 0]
		}, 1, 1);
		
		Crafty.sprite(16, 16, 'assets/sprites/treasure.png', {
			TreasureSprite: [0, 0]
		}, 1, 1);

		Crafty.audio.add({
			alert: ["assets/sound/alert.mp3"],
			die: ["assets/sound/die.mp3"],
			gameOverMusic: ["assets/music/GameOver.mp3"]
		});
	}
};

		