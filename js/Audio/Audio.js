MINION4.Audio = function(config){
	this.config = config;
};

MINION4.Audio.prototype = {
	constructor: MINION4.Audio,
	
	playMusic: function(key, times) {
		if(times == undefined) {
			times = -1;
		}
		if(this.config.music)
		{
			Crafty.audio.play(key, times);
		}
	},
	
	playSound: function(key) {
		if(this.config.sound)
		{
			Crafty.audio.play(key);
		}
	}
}