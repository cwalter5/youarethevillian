MINION4.ConfigurationBuilder = function() {
};

MINION4.ConfigurationBuilder.prototype = {
	constructor: MINION4.ConfigurationBuilder,

	build: function() {
		var config = new Object();
		config.skipIntro = false;
		config.audio = new Object();
		config.audio.music = true;
		config.audio.sound = false;
		return config;
	}
};