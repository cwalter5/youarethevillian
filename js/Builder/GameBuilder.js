MINION4.GameBuilder = function(io) {
	this.io = io;
};

MINION4.GameBuilder.BlockSize = 16;

MINION4.GameBuilder.Physical = "Physical";

MINION4.GameBuilder.prototype = {
	constructor: MINION4.GameBuilder,
	
	createCamera: function() {			
		return Crafty.e("Camera");
	},

	createBlock: function(x, y, type) {
		var block = Crafty.e(type);
		block.attr({x: x * MINION4.GameBuilder.BlockSize, y: y * MINION4.GameBuilder.BlockSize})
		return block;
	},
	
	createHero: function(x, y) {
		var hero = Crafty.e("Hero");
		hero.attr({x: x * MINION4.GameBuilder.BlockSize, y: y * MINION4.GameBuilder.BlockSize});
		Crafty.map.insert(hero);
		return hero;
	},
	
	createPlayer: function(x, y, onDead, onWin) {
		var player = Crafty.e("Player");
        player.attr({x: x * MINION4.GameBuilder.BlockSize, y: y * MINION4.GameBuilder.BlockSize});
		player.onDead = onDead;
		player.onWin = onWin;
		this.createCamera().focus(player);
		Crafty.map.insert(player);
		return player;
	},
	
	createLevel: function(config) {
		var that = this;
		
		that.io.audio.playMusic(config.music, -1);
			
		Crafty.background(MINION4.Globals.Background);
		
		var x = config.x;
		var y = config.y;
		$.each(config.map, function(y, row) {
			$.each(row.split(','), function(x, cell) {
				var componentType = that.getComponentTypeFromMap(cell);
				switch(componentType) {
					case "Empty":
						break;
					case "Player":
						that.createPlayer(x, y, config.fail, config.win);
						break;
					case "Hero":
						that.createHero(x, y);
						break;
					default:
						that.createBlock(x, y, componentType);
						break;
				}
			});
		});
		
		var title = Crafty.e("InfoText")
			.text(config.name);
		title.x = config.x;
		title.y = config.y;
	},
	
	getComponentTypeFromMap: function(cell) {
		cell = cell.toUpperCase();
		switch(cell) {
			case "S":
				return "Stone";
			case "L":
				return "Lava";
			case "P":
				return "Player";
			case "H":
				return "Hero";
			case "T":
				return "Treasure";
			case "E":
				return "Exit";
			default:
				return "Empty";
		}
	}
};

