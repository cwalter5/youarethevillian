MINION4.SceneBuilder = function(document, config, io) {
	this.config = config;
	this.io = io;
	var that = this;

	var gameBuilder = new MINION4.GameBuilder(io);
	
	var gameOver = function() {
		that.load(MINION4.SceneBuilder.GameOver);
	};

	Crafty.scene(MINION4.SceneBuilder.Start, new MINION4.Start(io).create(document, 
		function() {
			that.load(MINION4.SceneBuilder.Intro);
		}));
		
	Crafty.scene(MINION4.SceneBuilder.Intro, new MINION4.Intro(io).create(document, 
		function() {
			that.load(MINION4.SceneBuilder.Level1);
		}));
		
	Crafty.scene(MINION4.SceneBuilder.Level1, new MINION4.Level1(io).create(document, gameBuilder, 
		function() {
			that.load(MINION4.SceneBuilder.PreLevel2)
		}, gameOver));
		
	Crafty.scene(MINION4.SceneBuilder.PreLevel2, new MINION4.PreLevel2(io).create(document, 
		function() {
			that.load(MINION4.SceneBuilder.Level2);
		}));
		
	Crafty.scene(MINION4.SceneBuilder.Level2, new MINION4.Level2(io).create(document, gameBuilder, function() {
			that.load(MINION4.SceneBuilder.PreLevel3)
		}, gameOver));
	
	Crafty.scene(MINION4.SceneBuilder.PreLevel3, new MINION4.PreLevel3(io).create(document, 
		function() {
			that.load(MINION4.SceneBuilder.Level3);
		}));
	
	Crafty.scene(MINION4.SceneBuilder.Level3, new MINION4.Level3(io).create(document, gameBuilder, gameOver, function() {
			that.load(MINION4.SceneBuilder.YouWin)
		}));
		
	Crafty.scene(MINION4.SceneBuilder.YouWin, new MINION4.YouWin(io).create(document, 
		function() {
			that.load(MINION4.SceneBuilder.Start);
		}));
	
	Crafty.scene(MINION4.SceneBuilder.GameOver, new MINION4.GameOver(io).create(document, 
		function() {
			that.load(MINION4.SceneBuilder.Start);
		}));
};

//Menus
MINION4.SceneBuilder.Start = "start";

//Cutscenes
MINION4.SceneBuilder.Intro = "intro";
MINION4.SceneBuilder.PreLevel2 = "prelevel2";
MINION4.SceneBuilder.PreLevel3 = "prelevel3";
MINION4.SceneBuilder.GameOver = "gameover";
MINION4.SceneBuilder.YouWin = "youwin";

//Game
MINION4.SceneBuilder.Level1 = "level1";
MINION4.SceneBuilder.Level2 = "level2";
MINION4.SceneBuilder.Level3 = "level3";

MINION4.SceneBuilder.prototype = {
	constructor: MINION4.SceneBuilder,
	
	start: function() {
		if(this.config.skipIntro) {
			this.load(MINION4.SceneBuilder.Level1);
		}
		else {
			this.load(MINION4.SceneBuilder.Start);
		}
	},
	
	load: function(sceneName) {
		Crafty.audio.stop();
		Crafty.scene(sceneName);
	}
};



