Crafty.c("Camera",{
	init: function() {  },
	
	//Error in camera code as it does not follow the player when jumping unless the player is also moving.
	focus: function(obj) {
		this.set(obj);
		var that = this;
		obj.bind("Moved", function(location) { that.set(location); });
	},
	
	set: function(obj) {
		Crafty.viewport.x = -obj.x + Crafty.viewport.width / 2;
		Crafty.viewport.y = -obj.y + Crafty.viewport.height / 2;
	}
});