Crafty.c("CutSceneControls",{
	init: function() {
		var that = this;
		
		this.requires("2D, DOM, Mouse, Keyboard");
		this.x = 0;
		this.y = 0;
		this.w = 9000;
		this.h = 9000;

		this.bind('Click', function() {
			that.onSceneEnd();
		});
		
		this.bind('KeyDown', function(keyPress) {
			if(keyPress.key == Crafty.keys.SPACE) {
				that.onSceneEnd();
			}
		});
	}
});