Crafty.c("Exit", {
	init: function() {
		this.requires("2D, Canvas, Color, Collision");
		this.color("#FFFFFF");
		this.w = 16;
		this.h = 16;
	}
});