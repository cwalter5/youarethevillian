Crafty.c("Hero", {
	init: function() {
		var that = this;
		
		this.requires("Gravity, Kills, Killable, Physical");
		this.w = 24;
		this.h = 64;
		this.scanDistance = 500;
		this.moveRate = 1;
		this.color("#FF00FF");
		this.gravity("Physical");
		
		this.mode = this.scanMode;
		this.bind('EnterFrame', function() {
			that.mode();
		});
		
		this.spotTaunts = ["Die foul demon!!"];
		this.dyingTaunts = ["Oh the burning, it burns!!"];
		this.deadTaunts = ["Gurgle"];
	},
	
	dying: function() {
		this.h -= 1;
		if(this.h == 0) {
			this.taunt(this.deadTaunts);
			this.dead();
		}
	},
	
	followMode: function() {
		var moveDirection = -1;
		if(this.x < this.following.x) {
			moveDirection = 1;
		}
		this.x += (moveDirection * this.moveRate);
	},
	
	onDie: function() {
		if(this.mode != this.dying) {
			this.taunt(this.dyingTaunts);
			this.mode = this.dying;
		}
	},
	
	scanMode: function() {
		var foundEntities = Crafty("Minion");
		var closestEntity;
		var closestDistance = this.scanDistance;
		for(var i = 0; i < foundEntities.length; i++) {
			var foundEntity = Crafty(foundEntities[i]);
			if(Crafty.math.distance(foundEntity.x, foundEntity.y, this.x, this.y) < closestDistance) {
				closestEntity = foundEntity;
				this.taunt(this.spotTaunts);
			}
		}
		
		if(closestEntity != undefined) {
			this.following = closestEntity;
			this.mode = this.followMode;
			Crafty.audio.play("alert");
		}
	},
	taunt: function(taunts) {
		Crafty.e("Taunt").attr({x: this.x, y: this.y})
			.text(taunts[0]);
	}
});