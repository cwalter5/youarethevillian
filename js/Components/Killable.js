Crafty.c("Killable", {
	init: function() {
		this.requires("Collision");
		this.onHit("Kills", function() { this.hurt(1) });
		this.hp = 1;
	},
	
	hurt: function(damage) {
		this.hp -= damage;
		if(this.hp < 0) {
			this.die();
		}
	},
	
	die: function() {
		if(this.onDie != undefined) {
			this.onDie();
		} else {
			this.dead();
		}
	},
	
	dead: function() {
		if(this.onDead != undefined) {
			this.onDead();
		} 
		this.destroy();
	}
});