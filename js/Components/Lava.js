Crafty.c("Lava", {
	init: function() {
		this.requires("2D, Canvas, Color, Kills, LavaSprite");
		this.color("#FF0000");
		this.w = 16;
		this.h = 16;
	}
});