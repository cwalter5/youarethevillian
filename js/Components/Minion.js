Crafty.c("Minion", {
	init: function() {
		var that = this;
		
		this.requires("Gravity, Killable, Physical");
		this.w = 16;
		this.h = 48;
		this.color("#AA4400");
		this.gravity("Physical");
		
		this.dyingTaunts = ["Ow me have boo boo"];
		this.deadTaunts = ["Wargargle"];
		
		this.bind('EnterFrame', function() {
			if(that.mode != undefined) {
				that.mode();
			}
		});
	},
	
	dying: function() {
		this.h -= 1;
		if(this.h == 0) {
			this.taunt(this.deadTaunts);
			this.dead();
		}
	},
	
	onDie: function() {
		if(this.mode != this.dying) {
			this.taunt(this.dyingTaunts);
			this.mode = this.dying;
			Crafty.audio.play("die");
		}
	},
	taunt: function(taunts) {
		Crafty.e("Taunt").attr({x: this.x, y: this.y})
			.text(taunts[0]);
	}
});