Crafty.c("Physical", {
	init: function() {
		this.requires("2D, Canvas, Color, Collision");
		this.bind('Moved', function(from) {
			if(this.hit('Physical')) {
				this.attr({x: from.x, y:from.y});
			}
		});
	}
});