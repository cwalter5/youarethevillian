Crafty.c("Player", {
	init: function() {
		this.requires("Twoway, Minion");
		this.twoway(3, 4);
		this.bind('Moved', function(from) {
			if(this.hit('Exit')) {
				this.onWin();
			}
		});
	}
});