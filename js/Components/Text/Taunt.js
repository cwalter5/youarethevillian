Crafty.c("Taunt",{
	init: function() {
		var that = this;
		
		this.requires("2D, DOM, Text, Time");
		this.css({'color':'#FFFFFF', 'font-size': 'small'});
		this.w = 500;
		this.timeout(function() {
			that.destroy();
		}, 
		2000);
	}
});