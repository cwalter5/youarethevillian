MINION4.Game = function(screen, document, canvas) {
	new MINION4.AssetLoader().load();

	Crafty.init(document.width, document.height);
	
	var config = new MINION4.ConfigurationBuilder().build();
	
	var io = new Object();
	io.audio = new MINION4.Audio(config.audio);
	
	var sceneBuilder = new MINION4.SceneBuilder(document, config, io);
	sceneBuilder.start();
}

MINION4.Game.prototype = {
	constructor: MINION4.Game
};