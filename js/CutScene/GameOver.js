MINION4.GameOver = function(io) {
	this.io = io
};

MINION4.GameOver.prototype = {
	constructor: MINION4.GameOver,
	
	create: function(document, onRestart) {
		var that = this;
		return function() {
			Crafty.viewport.x = 0;
			Crafty.viewport.y = 0;
			that.io.audio.playMusic("gameOverMusic", 1);
		
			var leftAlign = 25;
			var width = 500;
			
			Crafty.background(MINION4.Globals.Background);
			var title = Crafty.e("InfoText")
				.css({'font-size': 'xx-large'})
				.text("Game Over");
			title.y = title.x;
			
			var instruction = Crafty.e("InfoText")
				.text("Press space to restart");
			instruction.y = document.height - 50;
			
			Crafty.e("CutSceneControls").onSceneEnd = onRestart;
		};
	}
};