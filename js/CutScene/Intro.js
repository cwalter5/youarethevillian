MINION4.Intro = function(io) {
	this.io = io
	Crafty.audio.add({
		introMusic: ["assets/music/Intro.mp3"],
	});
};

MINION4.Intro.prototype = {
	constructor: MINION4.Intro,
	
	create: function(document, onSceneEnd) {
		var that = this;
		return function() {
			Crafty.viewport.x = 0;
			Crafty.viewport.y = 0;
			that.io.audio.playMusic("introMusic", -1);
		
			Crafty.background(MINION4.Globals.Background);
			
			var title = Crafty.e("InfoText")
				.text("Minion #2: Master greatest ever. He tell me no one go in shiny room. I make sure no one go in shiny room. I please master. Then maybe master give me shiny. OOOOOOOOooooh shiny!!. Shiny, shiny, shiny! \nShiny, sparkle like, fire river. Mom say no play fire river, it bite. Me no listen to mom cause master say I can play fire river. In fact master say I'd have jump in fire river before he give me shiny. But master say no let other people touch shiny first. So confused.\n Oh no me in shiny room. Master say me have play in fire river if I ever in fire room. OOOh, maybe he give me shiny then....");
			title.y = title.x;
			
			var instruction = Crafty.e("InfoText")
				.text("Press space to continue");
			instruction.y = document.height - 50;
			
			Crafty.e("CutSceneControls").onSceneEnd = onSceneEnd;
		};
	}
};