MINION4.PreLevel2 = function(io) {
	this.io = io
};

MINION4.PreLevel2.prototype = {
	constructor: MINION4.PreLevel2,
	
	create: function(document, onSceneEnd) {
		var that = this;
		return function() {
			Crafty.viewport.x = 0;
			Crafty.viewport.y = 0;

			Crafty.background(MINION4.Globals.Background);
			
			var title = Crafty.e("InfoText")
				.text("Minion #3: Master greatest ever. He keep me safe from shiny man. Shiny man bad. He want take shinies away from master. Cause he greedy and stupid. Stupid shiny man. They master's shiny. Master say if I ever see shiny man, yell real loud so he chase me into fire river. Stupid shiny man. He dumb. He walk right into fire river cause he no see good. He have big ugly hat. No wonder he no see. Master said I could have shiny hat once, but me say no. Me no want to be like shiny man. Stupid shiny man. Wait... (sniff, sniff). Me smell something. Me think me smell shiny man. Me make shiny man walk into fire river. Me make sure fire river eat him. Then master love me.");
			title.y = title.x;
			
			var instruction = Crafty.e("InfoText")
				.text("Press space to continue");
			instruction.y = document.height - 50;
			
			Crafty.e("CutSceneControls").onSceneEnd = onSceneEnd;
		};
	}
};