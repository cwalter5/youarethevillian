MINION4.PreLevel3 = function(io) {
	this.io = io
};

MINION4.PreLevel3.prototype = {
	constructor: MINION4.PreLevel3,
	
	create: function(document, onSceneEnd) {
		var that = this;
		return function() {
			Crafty.viewport.x = 0;
			Crafty.viewport.y = 0;

			Crafty.background(MINION4.Globals.Background);
			
			var title = Crafty.e("InfoText")
				.text("Minion #4: Master greatest ever. He say me should walk towards white light. He says it good for me. He says I should find more shiny sparklies and bring them to him. I love master. I do that.");
			title.y = title.x;
			
			var instruction = Crafty.e("InfoText")
				.text("Press space to continue");
			instruction.y = document.height - 50;
			
			Crafty.e("CutSceneControls").onSceneEnd = onSceneEnd;
		};
	}
};