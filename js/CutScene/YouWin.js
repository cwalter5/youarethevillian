MINION4.YouWin = function(io) {
};

MINION4.YouWin.prototype = {
	constructor: MINION4.YouWin,
	
	create: function(document, onRestart) {
		var that = this;
		return function() {
			Crafty.viewport.x = 0;
			Crafty.viewport.y = 0;
		
			var leftAlign = 25;
			var width = 500;
			
			Crafty.background(MINION4.Globals.Background);
			var title = Crafty.e("InfoText")
				.css({'font-size': 'xx-large'})
				.text("You Win");
			title.y = title.x;
			
			Crafty.background(MINION4.Globals.Background);
			var info = Crafty.e("InfoText")
				.text("Minion #4: Me smartest minion ever. Me no die in fire river. Me smarter than shiny man. Now master give me shiny.");
			info.y = info.x + 50;
			
			var instruction = Crafty.e("InfoText")
				.text("Press any key to restart");
			instruction.y = document.height - 50;
			
			Crafty.e("CutSceneControls").onSceneEnd = onRestart;
		};
	}
};