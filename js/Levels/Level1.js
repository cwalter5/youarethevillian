MINION4.Level1 = function(io) {
	this.io = io
	Crafty.audio.add({
		level1Music: ["assets/music/Level1.mp3"],
	});
};

MINION4.Level1.prototype = {
	constructor: MINION4.Level1,
	
	create: function(document, gameBuilder, onFail, onWin) {
		var that = this;
		return function() {
			gameBuilder.createLevel({
				x: -25, 
				y: -25, 
				map: MINION4.Level1.Map,
				name: "No minions in the treasure room.",
				music: "level1Music", 
				fail: onFail
			});
		};
	},
};