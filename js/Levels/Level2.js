MINION4.Level2 = function(io) {
	this.io = io
	Crafty.audio.add({
		level1Music: ["assets/music/Level1.mp3"],
	});
};

MINION4.Level2.prototype = {
	constructor: MINION4.Level2,
	
	create: function(document, gameBuilder, onFail, onWin) {
		var that = this;
		return function() {
			gameBuilder.createLevel({
				x: -25, 
				y: -25, 
				map: MINION4.Level2.Map, 
				music: "level1Music",
				name: "Stupid shiny man.",
				fail: onFail
			});
		};
	},
};