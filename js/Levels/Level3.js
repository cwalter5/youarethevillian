MINION4.Level3 = function(io) {
	this.io = io
	Crafty.audio.add({
		level1Music: ["assets/music/Level1.mp3"],
	});
};

MINION4.Level3.prototype = {
	constructor: MINION4.Level3,
	
	create: function(document, gameBuilder, onFail, onWin) {
		var that = this;
		return function() {
			gameBuilder.createLevel({
				x: -25, 
				y: -25, 
				map: MINION4.Level3.Map, 
				music: "level1Music",
				name: "Minions don't always have to die.",
				fail: onFail,
				win: onWin
			});
		};
	},
};