MINION4.Start = function(io) {
	this.io = io;

	Crafty.audio.add({
		startMusic: ["assets/music/Start.mp3"],
	});
};

MINION4.Start.prototype = {
	constructor: MINION4.Start,
	
	create: function(document, onStart) {
		var that = this;
		return function() {
			Crafty.viewport.x = 0;
			Crafty.viewport.y = 0;
			that.io.audio.playMusic("startMusic");
			
			Crafty.background(MINION4.Globals.Background);
			var title = Crafty.e("InfoText")
				.css({'font-size': 'xx-large'})
				.text("Minion #4");
			title.y = title.x;
			
			var instruction = Crafty.e("InfoText")
				.text("Press space to start");
			instruction.y = document.height - 50;
			
			Crafty.e("CutSceneControls").onSceneEnd = onStart;
		};
	}
};